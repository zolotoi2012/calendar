package logger

import (
	"go.uber.org/zap"
)

func Log() *zap.SugaredLogger {
	logger := zap.NewExample()
	defer func(logger *zap.Logger) {
		err := logger.Sync()
		if err != nil {
			return
		}
	}(logger)

	return logger.Sugar()
}