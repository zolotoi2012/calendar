package main

import (
	"fmt"
	"github.com/joho/godotenv"
	"github.com/workshops/calendar/internal/repository/postgre"
	handler "github.com/workshops/calendar/internal/server/http"
	"github.com/workshops/calendar/internal/services/calendar"
	"github.com/workshops/calendar/internal/services/validator"
	"github.com/workshops/calendar/pb"
	"github.com/workshops/calendar/pkg/logger"
	"google.golang.org/grpc"
	"log"
	"net"
	"os"
)

func main() {
	lis, err := net.Listen("tcp", fmt.Sprintf(":%d", 9090))
	if err != nil {
		log.Fatalf("failed to listen: %v", err)
	}

	validator.NewValidator()
	repo, err := postgre.NewRepository(GetDsn())
	//m := auth.NewMiddleware()
	opts := make([]grpc.ServerOption, 0)
	//opts = append(opts, grpc.ChainUnaryInterceptor(m.AuthGrpc()))

	grpcServer := grpc.NewServer(opts...)

	grpcHandler := handler.GrpcHandler{Service: calendar.NewService(repo)}
	// registering specific handlers for this server
	pb.RegisterEventServiceServer(grpcServer, &grpcHandler)
	log.Println("starting server")

	if err := grpcServer.Serve(lis); err != nil {
		log.Fatalf("failed to serve: %s", err)
	}
}

func GetDsn() string{
	err := godotenv.Load(".env")
	if err != nil {
		logger.Log().Fatal(err.Error())
	}

	return os.Getenv("DATABASE_DSN")
}