package main

import (
	_ "github.com/golang-migrate/migrate/v4/database/postgres"
	_ "github.com/golang-migrate/migrate/v4/source/github"
	"github.com/joho/godotenv"
	"github.com/julienschmidt/httprouter"
	"github.com/workshops/calendar/internal/repository/postgre"
	routes "github.com/workshops/calendar/internal/server/http"
	"github.com/workshops/calendar/internal/services/calendar"
	"github.com/workshops/calendar/internal/services/validator"
	"github.com/workshops/calendar/pkg/logger"
	"net"
	"net/http"
	"os"
	"time"
)

func main() {
	logger.Log().Info("Logger works!")
	router := httprouter.New()

	repo, err := postgre.NewRepository(GetDsn())

	if err != nil {
		logger.Log().Fatal(err.Error())
	}

	validator.NewValidator()
	//run migrations
	//db, err := sql.Open("postgres", "postgres://localhost:5432/database?sslmode=enable")
	//driver, err := postgres.WithInstance(db, &postgres.Config{})
	//m, err := migrate.NewWithDatabaseInstance(
	//	"../../../migrations",
	//	"postgres", driver)
	//m.Up()

	HandleHttp(router, repo)
}

func HandleHttp(router *httprouter.Router, repo *postgre.Repository) {
	httpHandler := routes.NewHandler(calendar.NewService(repo))
	httpHandler.RegisterRoutes(router)

	listener, err := net.Listen("tcp", ":8080")
	if err != nil {
		logger.Log().Info(err)
		return
	}

	server := &http.Server{
		Handler: router,
		WriteTimeout: 15 * time.Second,
		ReadTimeout: 15 * time.Second,
	}

	e := server.Serve(listener)
	if e != nil {
		return
	}
}

func GetDsn() string{
	err := godotenv.Load(".env")
	if err != nil {
		logger.Log().Fatal(err.Error())
	}

	return os.Getenv("DATABASE_DSN")
}

