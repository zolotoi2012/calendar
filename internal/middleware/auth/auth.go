package auth

import (
	"context"
	"encoding/base64"
	"github.com/julienschmidt/httprouter"
	"github.com/workshops/calendar/internal/services/calendar"
	"google.golang.org/grpc"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/metadata"
	"google.golang.org/grpc/status"
	"net/http"
	"strings"
)

type Middleware struct {}

const (
	authHeader = "authorization"
	bearerAuth = "bearer"
)

func NewMiddleware() *Middleware {
	return &Middleware{}
}

func (m *Middleware) AuthHttp(h httprouter.Handle) httprouter.Handle {
	return func(w http.ResponseWriter, r *http.Request, ps httprouter.Params) {
		clientToken := r.Header.Get(authHeader)
		if clientToken == "" {
			w.WriteHeader(http.StatusForbidden)
			w.Write([]byte("No authorization header provided"))
			return
		}

		extractedToken := strings.Split(clientToken, "Bearer ")

		if len(extractedToken) == 2 {
			clientToken = strings.TrimSpace(extractedToken[1])
		} else {
			w.WriteHeader(http.StatusBadRequest)
			w.Write([]byte("Incorrect Format of Authorization Token"))
			return
		}

		jwtWrapper := calendar.JwtWrapper{
			SecretKey: "calendar",
			Issuer: "AuthService",
		}

		_, err := jwtWrapper.ValidateToken(clientToken)
		if err != nil {
			w.WriteHeader(http.StatusUnauthorized)
			w.Write([]byte("Incorrect Authorization Token"))
			return
		}

		h(w, r, ps)
	}
}

func (m *Middleware) AuthGrpc() grpc.UnaryServerInterceptor {
	return func(ctx context.Context, req interface{}, info *grpc.UnaryServerInfo, handler grpc.UnaryHandler) (resp interface{}, err error) {
		credentials := m.getAuthCredentials(ctx)
		if credentials == "" {
			return nil, status.Error(codes.Unauthenticated, "unauthenticated")
		}

		decoded, err := base64.StdEncoding.DecodeString(credentials)
		if err != nil {
			return nil, status.Error(codes.Unauthenticated, "can't parse credentials")
		}

		data := strings.Split(string(decoded), ":")
		if len(data) != 2 {
			return nil, status.Error(codes.Unauthenticated, "can't parse credentials")
		}

		ctx = context.WithValue(ctx, "user", data[0])
		return handler(ctx, req)
	}
}

func (m *Middleware) getAuthCredentials(ctx context.Context) string {
	md, ok := metadata.FromIncomingContext(ctx)
	if !ok {
		return ""
	}

	values := md.Get(authHeader)
	if len(values) == 0 {
		return ""
	}

	fields := strings.SplitN(values[0], " ", 2)
	if len(fields) < 2 {
		return ""
	}

	if !strings.EqualFold(fields[0], bearerAuth) {
		return ""
	}
	return fields[1]
}