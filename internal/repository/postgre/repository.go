package postgre

import (
	"database/sql"
	"fmt"
	_ "github.com/lib/pq"
	"github.com/workshops/calendar/internal/entity"
	"github.com/workshops/calendar/pkg/logger"
	"time"
)

type Repository struct {
	db *sql.DB
}

func NewRepository(dsn string) (*Repository, error) {
	db, err := sql.Open("postgres", dsn)
	if err != nil {
		logger.Log().Warn(err.Error())
		return nil, err
	}

	if err := db.Ping(); err != nil {
		logger.Log().Warn(err.Error())
		return nil, err
	}

	return &Repository{
		db: db,
	}, nil
}

func (r *Repository) Close() {
	err := r.db.Close()
	if err != nil {
		logger.Log().Warn(err.Error())
		return
	}
}

func (r *Repository) GetEvents() (res []entity.Event, err error) {
	query, err := r.db.Query("SELECT * FROM events")
	if err != nil {
		return nil, err
	}
	defer query.Close()

	for query.Next() {
		var event entity.Event
		err = query.Scan(&event.ID, &event.Title, &event.Description, &event.Duration, &event.EventTimezone, &event.Notes, &event.UserId, &event.StartDate)
		if err != nil {
			return
		}
		res = append(res, event)
	}

	return res, nil
}

func (r *Repository) GetEventById(id string) (res []entity.Event, err error) {
	sqlStatement := "SELECT * FROM events WHERE id = $1"
	query := r.db.QueryRow(sqlStatement, id)

	var event entity.Event
	err = query.Scan(&event.ID, &event.Title, &event.Description, &event.Duration, &event.EventTimezone, &event.Notes, &event.UserId, &event.StartDate)
	if err != nil {
		return res, err
	}

	res = append(res, event)
	return res, nil
}

func (r *Repository) Create(event entity.Event) string {
	sqlStatement := "INSERT INTO events (title, description, notes, start_date, duration, user_id, event_timezone)" +
		"VALUES ($1, $2, $3, $4, $5, $6, $7) RETURNING id"

	r.db.QueryRow(sqlStatement, event.Title, event.Description, event.Notes, event.StartDate, event.Duration, event.UserId, event.EventTimezone)

	return "Success"
}

func (r *Repository) Delete(id string) string {
	var eventId string
	sqlStatement := "DELETE FROM events WHERE id=$1 RETURNING id"
	err := r.db.QueryRow(sqlStatement, id).Scan(&eventId)

	if err != nil {
		logger.Log().Warn("Error while delete event: " + id)
		return "Error while delete event"
	}

	return eventId
}

func (r *Repository) Update(id string, event entity.Event) string {
	sqlStatement := "UPDATE events SET title=$2, description=$3, notes=$4, start_date=$5, duration=$6 WHERE id=$1"
	res, err := r.db.Exec(sqlStatement, id, event.Title, event.Description, event.Notes, time.Now()/** event.StartDate **/, event.Duration)
	if err != nil {
		return err.Error()
	}

	rowsAffected, err := res.RowsAffected()
	if err != nil {
		return err.Error()
	}

	return fmt.Sprintf("Updated successfully. Fields changed: %d", rowsAffected)
}

func (r *Repository) Login(user entity.User) (entity.User, error) {
	var userEntity entity.User
	sqlStatement := "SELECT * FROM users WHERE email=$1"
	query := r.db.QueryRow(sqlStatement, user.Email)
	err := query.Scan(&userEntity.Id, &userEntity.Email, &userEntity.Password)
	if err != nil {
		return userEntity, err
	}

	return userEntity, nil
}

func (r *Repository) Register(user entity.User) (string, error) {
	sqlStatement := "INSERT INTO users (email, password) VALUES ($1, $2) RETURNING id"
	var id string
	err := r.db.QueryRow(sqlStatement, user.Email, user.Password).Scan(&id)
	if err != nil {
		return "", err
	}

	return id, nil
}
