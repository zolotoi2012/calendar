package entity

import (
	"database/sql"
	"time"
)

type Event struct {
	ID          int64     `json:"id"`
	UserId      int64     `validate:"numeric" json:"user_id"`
	Title       string    `validate:"alphanum" json:"title"`
	Description string    `validate:"alphanum" json:"description"`
	StartDate   time.Time `json:"start_date"`
	Duration    int64     `validate:"numeric" json:"duration"`
	Notes       sql.NullString    `json:"notes"`
	EventTimezone    string 	  `json:"timezone"`
}

type EventFilter struct {
	Title string
	Timezone string
	DateFrom string
	DateTo string
	TimeFrom string
	TimeTo string
}

func NewEventFilter() *EventFilter {
	return &EventFilter{}
}

func (e *EventFilter) Filter(event Event) bool {
	if e.Title != "" {
		if e.Title != event.Title {
			return false
		}
	}

	if e.Timezone != "" {
		if e.Timezone != event.EventTimezone {
			return false
		}
	}

	//hours, minutes, _ := event.StartDate.Clock()
	//eventTime := fmt.Sprintf("%d%02d", hours, minutes)
	// 08:00 >= 10:00
	//
	//if e.TimeFrom != "" {
	//	e.TimeFrom
	//}

	return true
}
