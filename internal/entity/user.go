package entity

import (
	"golang.org/x/crypto/bcrypt"
)

type User struct {
	Id string `json:"id"`
	Email string `validate:"required,email" json:"email"`
	Password string `validate:"required" json:"password"`
	Token LoginResponse
}

type LoginResponse struct {
	Token string `json:"token"`
}

func (u *User) HashPassword(password string) error {
	bytes, err := bcrypt.GenerateFromPassword([]byte(password), 10)
	if err != nil {
		return err
	}

	u.Password = string(bytes)

	return nil
}

func (u *User) CheckPassword(providedPassword string) error {
	err := bcrypt.CompareHashAndPassword([]byte(u.Password), []byte(providedPassword))
	if err != nil {
		return err
	}

	return nil
}