package http

import (
	"context"
	"database/sql"
	"encoding/json"
	"github.com/gorilla/schema"
	"github.com/julienschmidt/httprouter"
	"github.com/workshops/calendar/internal/entity"
	"github.com/workshops/calendar/internal/middleware/auth"
	"github.com/workshops/calendar/internal/services/calendar"
	"github.com/workshops/calendar/pb"
	"google.golang.org/protobuf/types/known/timestamppb"
	"net/http"
	"strconv"
)

const (
	eventsURL = "/api/events"
	eventURL  = "/api/event/:id"
)

// will hold http routes and will registrate them
type Handler struct {
	service *calendar.Service
}

type GrpcHandler struct {
	pb.UnimplementedEventServiceServer
	Service *calendar.Service
}

func NewHandler(s *calendar.Service) *Handler {
	return &Handler{s}
}

func (h *Handler) RegisterRoutes(router *httprouter.Router) {
	m := auth.NewMiddleware()
	router.POST("/api/login", h.Login)
	router.POST("/api/register", h.Register)
	router.GET(eventsURL, m.AuthHttp(h.EventsList))
	router.GET(eventURL, h.GetEventById)//m.AuthHttp(h.GetEventById))
	router.POST(eventsURL, m.AuthHttp(h.CreateEvent))
	router.PUT(eventURL, m.AuthHttp(h.UpdateEvent))
	router.DELETE(eventURL, m.AuthHttp(h.DeleteEvent))
}

//each func only for get parameters and return responce
func (h *Handler) Login(w http.ResponseWriter, r *http.Request, params httprouter.Params) {
	var user entity.User
	err := json.NewDecoder(r.Body).Decode(&user)
	if err != nil {
		w.WriteHeader(http.StatusBadRequest)
		w.Write([]byte(err.Error()))
	}

	result, err := h.service.Login(user)
	if err != nil {
		w.WriteHeader(http.StatusBadRequest)
		w.Write([]byte(err.Error()))
	}

	marshal, err := json.Marshal(result)
	if err != nil {
		w.WriteHeader(http.StatusBadRequest)
		w.Write([]byte(err.Error()))
	}

	w.WriteHeader(http.StatusOK)
	w.Write(marshal)
}

func (h *Handler) Register(w http.ResponseWriter, r *http.Request, params httprouter.Params) {
	var user entity.User
	err := json.NewDecoder(r.Body).Decode(&user)
	if err != nil {
		w.WriteHeader(http.StatusBadRequest)
	}

	res, err := h.service.Register(user)
	if err != nil {
		w.WriteHeader(http.StatusBadRequest)
	}

	w.WriteHeader(http.StatusOK)
	w.Write([]byte(res))
}

func (h *Handler) EventsList(w http.ResponseWriter, r *http.Request, params httprouter.Params) {
	if err := r.ParseForm(); err != nil {
		w.WriteHeader(http.StatusBadRequest)
		return
	}

	eventFilter := new(entity.EventFilter)
	if err := schema.NewDecoder().Decode(eventFilter, r.Form); err != nil {
		w.WriteHeader(http.StatusBadRequest)
		return
	}

	events, err := h.service.GetEvents(eventFilter)
	if err != nil {
		w.WriteHeader(http.StatusBadRequest)
		return
	}

	marshal, err := json.Marshal(events)
	if err != nil {
		w.WriteHeader(http.StatusBadRequest)
		return
	}

	w.WriteHeader(http.StatusOK)
	w.Write(marshal)
}

func (h *Handler) GetEventById(w http.ResponseWriter, r *http.Request, params httprouter.Params) {
	event, err := h.service.GetEventById(params.ByName("id"))
	if err != nil {
		w.WriteHeader(http.StatusBadRequest)
		return
	}

	marshal, err := json.Marshal(event)
	if err != nil {
		w.WriteHeader(http.StatusBadRequest)
		return
	}
	w.WriteHeader(http.StatusOK)
	w.Write(marshal)
}

func (h *Handler) CreateEvent(w http.ResponseWriter, r *http.Request, params httprouter.Params) {
	var event entity.Event
	err := json.NewDecoder(r.Body).Decode(&event)
	if err != nil {
		w.WriteHeader(http.StatusBadRequest)
		return
	}

	w.WriteHeader(http.StatusOK)
	w.Write([]byte(h.service.Create(event)))
}

func (h *Handler) UpdateEvent(w http.ResponseWriter, r *http.Request, params httprouter.Params) {
	var event entity.Event
	err := json.NewDecoder(r.Body).Decode(&event)
	if err != nil {
		w.WriteHeader(http.StatusBadRequest)
		w.Write([]byte("Bad data"))
	}

	res := h.service.Update(params.ByName("id"), event)
	marshal, err := json.Marshal(res)
	if err != nil {
		w.WriteHeader(http.StatusBadRequest)
		return
	}

	w.WriteHeader(http.StatusOK)
	w.Write(marshal)
}

func (h *Handler) DeleteEvent(w http.ResponseWriter, r *http.Request, params httprouter.Params) {
	event := h.service.Delete(params.ByName("id"))

	marshal, err := json.Marshal(event)
	if err != nil {
		w.WriteHeader(http.StatusBadRequest)
		return
	}
	w.WriteHeader(http.StatusOK)
	w.Write(marshal)
}

func (g *GrpcHandler) GetById(ctx context.Context, in *pb.EventId) (*pb.Event, error) {
	event, err := g.Service.GetEventById(strconv.FormatInt(in.Id, 10))
	if err != nil {
		return &pb.Event{}, err
	}

	return &pb.Event{
		Id: event[0].ID,
		Title: event[0].Title,
		Description: event[0].Description,
		Timezone: event[0].EventTimezone,
		Notes: event[0].Notes.String,
		StartDate: timestamppb.New(event[0].StartDate),
		Duration: event[0].Duration,
		UserId: event[0].UserId,
	}, nil
}

func (g *GrpcHandler) GetList(ctx context.Context, in *pb.EventFilter) (*pb.EventsList, error) {
	var res []*pb.Event
	filter := entity.NewEventFilter()

	filter.Title = in.Title
	filter.Timezone = in.Timezone

	result, err := g.Service.GetEvents(filter)
	if err != nil {
		return &pb.EventsList{}, err
	}

	for _, v := range result {
		event := &pb.Event{
			Id: v.ID,
			Title: v.Title,
			Description: v.Description,
			Timezone: v.EventTimezone,
			UserId: v.UserId,
			Duration: v.Duration,
			Notes: v.Notes.String,
		}

		res = append(res, event)
	}

	return &pb.EventsList{
		EventList: res,
	}, nil
}

func (g *GrpcHandler) Create(ctx context.Context, in *pb.Event) (*pb.EventResponse, error) {
	event := entity.Event{
		Title: in.Title,
		Description: in.Description,
		EventTimezone: in.Timezone,
		StartDate: in.StartDate.AsTime(),
		Duration: in.Duration,
		UserId: in.UserId,
		Notes: sql.NullString{String: in.Notes, Valid: true},
	}

	response := g.Service.Create(event)
	return &pb.EventResponse{
		Msg: response,
	}, nil
}

func (g *GrpcHandler) Delete(ctx context.Context, in *pb.EventId) (*pb.EventResponse, error) {
	response := g.Service.Delete(strconv.FormatInt(in.Id, 10))
	return &pb.EventResponse{
		Msg: response,
	}, nil
}

func (g *GrpcHandler) Update(ctx context.Context, in *pb.Event) (*pb.EventResponse, error) {
	event := entity.Event{
		Title: in.Title,
		Description: in.Description,
		EventTimezone: in.Timezone,
		StartDate: in.StartDate.AsTime(),
		Duration: in.Duration,
		UserId: in.UserId,
		Notes: sql.NullString{String: in.Notes, Valid: true},
	}

	response := g.Service.Update(strconv.FormatInt(in.Id, 10), event)
	return &pb.EventResponse{
		Msg: response,
	}, nil
}
