package config

import (
	"database/sql"
	_ "github.com/lib/pq"
)

// Application holds application configuration values
type Application struct {
	DB *Database
}

type Database struct {
	DSN string `env:"DSN"`
	DB *sql.DB
}