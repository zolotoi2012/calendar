package validator

import (
	"github.com/go-playground/validator/v10"
	"github.com/workshops/calendar/internal/entity"
	"github.com/workshops/calendar/pkg/logger"
)

var validate *validator.Validate

func NewValidator() {
	validate = validator.New()
}

func ValidateEvent(event entity.Event) bool {
	err := validate.Struct(event)
	if err != nil {
		logger.Log().Warn(err.Error())
		return false
	}

	return true
}
