package calendar

import (
	"github.com/workshops/calendar/internal/entity"
	"github.com/workshops/calendar/internal/services/validator"
)

// Service holds calendar business logic and works with repository
type Service struct {
	repo Repository
}

func NewService(repo Repository) *Service {
	return &Service{repo: repo}
}

//go:generate mockgen -destination=repository_interface_mock.go -build_flags=-mod=mod -package=calendar github.com/workshops/calendar/internal/services/calendar Service
type Repository interface {
	GetEvents() ([]entity.Event, error)
	GetEventById(id string) ([]entity.Event, error)
	Create(event entity.Event) string
	Delete(id string) string
	Update(id string, event entity.Event) string
	Login(user entity.User) (entity.User, error)
	Register(user entity.User) (string, error)
}

func (s *Service) GetEvents(filter *entity.EventFilter) (res []entity.Event, err error) {
	events, err :=  s.repo.GetEvents()
	if err != nil {
		return nil, err
	}

	for i := range events {
		if filter.Filter(events[i]) {
			res = append(res, events[i])
		}
	}

	return res, nil
}

func (s *Service) GetEventById(id string) ([]entity.Event, error) {
	event, err := s.repo.GetEventById(id)

	if err != nil {
		return []entity.Event{}, err
	}

	return event, nil
}

func (s *Service) Delete(id string) string {
	return s.repo.Delete(id)
}

func (s *Service) Create(event entity.Event) string {
	if validator.ValidateEvent(event) {
		return s.repo.Create(event)
	}

	return "Incorrect data"
}

func (s *Service) Update(id string, event entity.Event) string {
	if validator.ValidateEvent(event) {
		return s.repo.Update(id, event)
	}

	return "Incorrect data"
}

func (s *Service) Login(user entity.User) (userEntity entity.User, err error) {
	userPassword := user.Password
	userEntity, err = s.repo.Login(user)
	if err != nil {
		return userEntity, err
	}

	err = userEntity.CheckPassword(userPassword)
	if err != nil {
		return userEntity, err
	}

	jwtWrapper := JwtWrapper{
		SecretKey:       "calendar",
		Issuer:          "AuthService",
		ExpirationHours: 24,
	}

	signedToken, err := jwtWrapper.GenerateToken(user.Email)
	if err != nil {
		return userEntity, err
	}

	tokenResponse := entity.LoginResponse{
		Token: signedToken,
	}

	userEntity.Token = tokenResponse

	return userEntity, nil
}

func (s *Service) Register(user entity.User) (string, error) {
	err := user.HashPassword(user.Password)
	if err != nil {
		return "", err
	}

	return s.repo.Register(user)
}