package calendar

import (
	reflect "reflect"
	"testing"
	"time"

	gomock "github.com/golang/mock/gomock"
	entity "github.com/workshops/calendar/internal/entity"
)

// MockRepository is a mock of Repository interface.
type MockRepository struct {
	ctrl     *gomock.Controller
	recorder *MockRepositoryMockRecorder
}

// MockRepositoryMockRecorder is the mock recorder for MockRepository.
type MockRepositoryMockRecorder struct {
	mock *MockRepository
}

// NewMockRepository creates a new mock instance.
func NewMockRepository(ctrl *gomock.Controller) *MockRepository {
	mock := &MockRepository{ctrl: ctrl}
	mock.recorder = &MockRepositoryMockRecorder{mock}
	return mock
}

// EXPECT returns an object that allows the caller to indicate expected use.
func (m *MockRepository) EXPECT() *MockRepositoryMockRecorder {
	return m.recorder
}

// Create mocks base method.
func (m *MockRepository) Create(event entity.Event) string {
	m.ctrl.T.Helper()
	ret := m.ctrl.Call(m, "Create", event)
	ret0, _ := ret[0].(string)
	return ret0
}

// Create indicates an expected call of Create.
func (mr *MockRepositoryMockRecorder) Create(event interface{}) *gomock.Call {
	mr.mock.ctrl.T.Helper()
	return mr.mock.ctrl.RecordCallWithMethodType(mr.mock, "Create", reflect.TypeOf((*MockRepository)(nil).Create), event)
}

// Delete mocks base method.
func (m *MockRepository) Delete(id string) string {
	m.ctrl.T.Helper()
	ret := m.ctrl.Call(m, "Delete", id)
	ret0, _ := ret[0].(string)
	return ret0
}

// Delete indicates an expected call of Delete.
func (mr *MockRepositoryMockRecorder) Delete(id interface{}) *gomock.Call {
	mr.mock.ctrl.T.Helper()
	return mr.mock.ctrl.RecordCallWithMethodType(mr.mock, "Delete", reflect.TypeOf((*MockRepository)(nil).Delete), id)
}

// GetEventById mocks base method.
func (m *MockRepository) GetEventById(id string) ([]entity.Event, error) {
	m.ctrl.T.Helper()
	ret := m.ctrl.Call(m, "GetEventById", id)
	ret0, _ := ret[0].([]entity.Event)
	ret1, _ := ret[1].(error)
	return ret0, ret1
}

// GetEventById indicates an expected call of GetEventById.
func (mr *MockRepositoryMockRecorder) GetEventById(id interface{}) *gomock.Call {
	mr.mock.ctrl.T.Helper()
	return mr.mock.ctrl.RecordCallWithMethodType(mr.mock, "GetEventById", reflect.TypeOf((*MockRepository)(nil).GetEventById), id)
}

// GetEvents mocks base method.
func (m *MockRepository) GetEvents() ([]entity.Event, error) {
	m.ctrl.T.Helper()
	ret := m.ctrl.Call(m, "GetEvents")
	ret0, _ := ret[0].([]entity.Event)
	ret1, _ := ret[1].(error)
	return ret0, ret1
}

// GetEvents indicates an expected call of GetEvents.
func (mr *MockRepositoryMockRecorder) GetEvents() *gomock.Call {
	mr.mock.ctrl.T.Helper()
	return mr.mock.ctrl.RecordCallWithMethodType(mr.mock, "GetEvents", reflect.TypeOf((*MockRepository)(nil).GetEvents))
}

// Login mocks base method.
func (m *MockRepository) Login(user entity.User) (entity.User, error) {
	m.ctrl.T.Helper()
	ret := m.ctrl.Call(m, "Login", user)
	ret0, _ := ret[0].(entity.User)
	ret1, _ := ret[1].(error)
	return ret0, ret1
}

// Login indicates an expected call of Login.
func (mr *MockRepositoryMockRecorder) Login(user interface{}) *gomock.Call {
	mr.mock.ctrl.T.Helper()
	return mr.mock.ctrl.RecordCallWithMethodType(mr.mock, "Login", reflect.TypeOf((*MockRepository)(nil).Login), user)
}

// Register mocks base method.
func (m *MockRepository) Register(user entity.User) (string, error) {
	m.ctrl.T.Helper()
	ret := m.ctrl.Call(m, "Register", user)
	ret0, _ := ret[0].(string)
	ret1, _ := ret[1].(error)
	return ret0, ret1
}

// Register indicates an expected call of Register.
func (mr *MockRepositoryMockRecorder) Register(user interface{}) *gomock.Call {
	mr.mock.ctrl.T.Helper()
	return mr.mock.ctrl.RecordCallWithMethodType(mr.mock, "Register", reflect.TypeOf((*MockRepository)(nil).Register), user)
}

// Update mocks base method.
func (m *MockRepository) Update(id string, event entity.Event) string {
	m.ctrl.T.Helper()
	ret := m.ctrl.Call(m, "Update", id, event)
	ret0, _ := ret[0].(string)
	return ret0
}

// Update indicates an expected call of Update.
func (mr *MockRepositoryMockRecorder) Update(id, event interface{}) *gomock.Call {
	mr.mock.ctrl.T.Helper()
	return mr.mock.ctrl.RecordCallWithMethodType(mr.mock, "Update", reflect.TypeOf((*MockRepository)(nil).Update), id, event)
}

func TestService_Create(t *testing.T) {
	ctrl := gomock.NewController(t)
	defer ctrl.Finish()

	event := entity.Event{
		Title: "Test",
		Description: "Test",
		UserId: 1,
		StartDate: time.Now(),
		Duration: 1,
		EventTimezone: "UTC",
	}

	tests := []struct {
		name                 string
		wantErr              bool
	}{
		{
			name: "Read error",
			wantErr: true,
		},
		{
			name: "No rows",
			wantErr: true,
		},
		{
			name: "Insert error",
			wantErr: true,
		},
		{
			name: "Success",
			wantErr: false,
		},
	}

	m := NewMockRepository(ctrl)

	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if tt.wantErr == true {
				m.
					EXPECT().
					Create(event).
					Return("Success")

				m.Create(event)
			} else {
				m.EXPECT().Create(event).Return("Incorrect Data")
				m.Create(event)
			}
		})
	}
}
